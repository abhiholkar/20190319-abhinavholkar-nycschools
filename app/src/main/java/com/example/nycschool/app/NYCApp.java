/*
 * Developed by Abhinav Holkar on 16/03/19 08:57.
 * Last modified 16/03/19 08:57.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.app;

import android.app.Application;

import com.example.nycschool.di.component.AppComponent;
import com.example.nycschool.di.component.DaggerAppComponent;
import com.example.nycschool.di.module.AppModule;

public class NYCApp extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
