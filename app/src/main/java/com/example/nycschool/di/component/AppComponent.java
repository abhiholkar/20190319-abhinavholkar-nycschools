/*
 * Developed by Abhinav Holkar on 16/03/19 09:35.
 * Last modified 16/03/19 09:35.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.di.component;

import com.example.nycschool.di.module.AppModule;
import com.example.nycschool.di.module.ViewModelModule;
import com.example.nycschool.view.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ViewModelModule.class})
public interface AppComponent {
    void inject(MainActivity activity);
}

