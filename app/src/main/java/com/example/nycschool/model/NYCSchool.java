package com.example.nycschool.model;

import android.os.Parcel;
import android.os.Parcelable;

public class NYCSchool implements Parcelable {

    public static final Parcelable.Creator<NYCSchool> CREATOR = new Parcelable.Creator<NYCSchool>() {
        @Override
        public NYCSchool createFromParcel(Parcel source) {
            return new NYCSchool(source);
        }

        @Override
        public NYCSchool[] newArray(int size) {
            return new NYCSchool[size];
        }
    };
    private SchoolData schoolData;
    private SchoolSatData schoolSatData;

    public NYCSchool() {
    }

    protected NYCSchool(Parcel in) {
        this.schoolData = in.readParcelable(SchoolData.class.getClassLoader());
        this.schoolSatData = in.readParcelable(SchoolSatData.class.getClassLoader());
    }

    public SchoolData getSchoolData() {
        return schoolData;
    }

    public void setSchoolData(SchoolData schoolData) {
        this.schoolData = schoolData;
    }

    public SchoolSatData getSchoolSatData() {
        return schoolSatData;
    }

    public void setSchoolSatData(SchoolSatData schoolSatData) {
        this.schoolSatData = schoolSatData;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.schoolData, flags);
        dest.writeParcelable(this.schoolSatData, flags);
    }
}
