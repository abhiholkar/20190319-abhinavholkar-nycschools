package com.example.nycschool.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class SchoolSatData implements Parcelable {

    public static final Parcelable.Creator<SchoolSatData> CREATOR = new Parcelable.Creator<SchoolSatData>() {
        @Override
        public SchoolSatData createFromParcel(Parcel source) {
            return new SchoolSatData(source);
        }

        @Override
        public SchoolSatData[] newArray(int size) {
            return new SchoolSatData[size];
        }
    };
    @Expose
    private String dbn;
    @SerializedName("num_of_sat_test_takers")
    private String numOfSatTestTakers;
    @SerializedName("sat_critical_reading_avg_score")
    private String satCriticalReadingAvgScore;
    @SerializedName("sat_math_avg_score")
    private String satMathAvgScore;
    @SerializedName("sat_writing_avg_score")
    private String satWritingAvgScore;
    @SerializedName("school_name")
    private String schoolName;

    public SchoolSatData() {
    }

    protected SchoolSatData(Parcel in) {
        this.dbn = in.readString();
        this.numOfSatTestTakers = in.readString();
        this.satCriticalReadingAvgScore = in.readString();
        this.satMathAvgScore = in.readString();
        this.satWritingAvgScore = in.readString();
        this.schoolName = in.readString();
    }

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getNumOfSatTestTakers() {
        return numOfSatTestTakers;
    }

    public void setNumOfSatTestTakers(String numOfSatTestTakers) {
        this.numOfSatTestTakers = numOfSatTestTakers;
    }

    public String getSatCriticalReadingAvgScore() {
        return satCriticalReadingAvgScore;
    }

    public void setSatCriticalReadingAvgScore(String satCriticalReadingAvgScore) {
        this.satCriticalReadingAvgScore = satCriticalReadingAvgScore;
    }

    public String getSatMathAvgScore() {
        return satMathAvgScore;
    }

    public void setSatMathAvgScore(String satMathAvgScore) {
        this.satMathAvgScore = satMathAvgScore;
    }

    public String getSatWritingAvgScore() {
        return satWritingAvgScore;
    }

    public void setSatWritingAvgScore(String satWritingAvgScore) {
        this.satWritingAvgScore = satWritingAvgScore;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.dbn);
        dest.writeString(this.numOfSatTestTakers);
        dest.writeString(this.satCriticalReadingAvgScore);
        dest.writeString(this.satMathAvgScore);
        dest.writeString(this.satWritingAvgScore);
        dest.writeString(this.schoolName);
    }
}
