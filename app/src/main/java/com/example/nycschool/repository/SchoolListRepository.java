/*
 * Developed by Abhinav Holkar on 16/03/19 10:49.
 * Last modified 16/03/19 10:49.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.repository;

import com.example.nycschool.model.SchoolData;
import com.example.nycschool.network.APIService;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class SchoolListRepository {

    private Retrofit retrofit;

    @Inject
    public SchoolListRepository(Retrofit retrofit) {
        this.retrofit = retrofit;
    }


    public Observable<List<SchoolData>> getNYCSchoolList() {

        return retrofit.create(APIService.class)
                .getSchoolList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


}
