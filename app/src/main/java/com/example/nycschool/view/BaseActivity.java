/*
 * Developed by Abhinav Holkar on 16/03/19 08:35.
 * Last modified 16/03/19 08:23.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
