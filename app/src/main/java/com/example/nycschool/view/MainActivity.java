/*
 * Developed by Abhinav Holkar on 16/03/19 08:35.
 * Last modified 16/03/19 08:34.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nycschool.R;
import com.example.nycschool.adapter.SchoolListAdapter;
import com.example.nycschool.app.NYCApp;
import com.example.nycschool.model.NYCSchool;
import com.example.nycschool.model.NYCSchoolListResponse;
import com.example.nycschool.utils.BundleConstants;
import com.example.nycschool.viewmodel.SchoolListViewModel;
import com.example.nycschool.viewmodel.ViewModelFactory;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements OnItemClickListener<NYCSchool> {


    @BindView(R.id.schoollist)
    RecyclerView schoolList;

    @BindView(R.id.pb)
    ProgressBar pb;

    @BindView(R.id.errorview)
    TextView errorView;

    @Inject
    ViewModelFactory viewModelFactory;

    private SchoolListViewModel schoolListViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //ButterKnife view injection
        ButterKnife.bind(this);

        //Dagger Dependency injection
        ((NYCApp) getApplication()).getAppComponent().inject(this);
        schoolListViewModel = ViewModelProviders.of(this, viewModelFactory).get(SchoolListViewModel.class);

        //Recylcer view layout
        schoolList.setLayoutManager(new LinearLayoutManager(this));

        //Observe the response
        schoolListViewModel.getSchoolList().observe(this, new Observer<NYCSchoolListResponse>() {
            @Override
            public void onChanged(@Nullable NYCSchoolListResponse nycSchoolListResponse) {
                pb.setVisibility(View.GONE);
                if (nycSchoolListResponse != null && nycSchoolListResponse.getNycSchoolList() != null) {
                    showData(nycSchoolListResponse);
                } else {
                    showError();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void showError() {
        schoolList.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
    }

    private void showData(@NonNull NYCSchoolListResponse response) {
        schoolList.setVisibility(View.VISIBLE);
        schoolList.setAdapter(new SchoolListAdapter(this, response.getNycSchoolList(), this));
        errorView.setVisibility(View.GONE);

    }

    @Override
    public void onItemClicked(NYCSchool item) {
        Intent detailIntent = new Intent(this, DetailActivity.class);
        detailIntent.putExtra(BundleConstants.NYC_SCHOOL_BUNDLE, item);
        startActivity(detailIntent);
        Toast.makeText(this, item.getSchoolData().getSchoolName(), Toast.LENGTH_SHORT).show();
    }
}
