/*
 * Developed by Abhinav Holkar on 16/03/19 09:58.
 * Last modified 16/03/19 09:58.
 * Copyright (c) 2019. All rights reserved.
 */

package com.example.nycschool.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.util.Log;

import com.example.nycschool.model.NYCSchool;
import com.example.nycschool.model.NYCSchoolListResponse;
import com.example.nycschool.usecase.GetSchoolListUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableObserver;

public class SchoolListViewModel extends ViewModel {

    private MutableLiveData<NYCSchoolListResponse> response;

    private GetSchoolListUseCase getSchoolListUseCase;

    @Inject
    public SchoolListViewModel(GetSchoolListUseCase getSchoolListUseCase) {
        this.getSchoolListUseCase = getSchoolListUseCase;
    }


    public MutableLiveData<NYCSchoolListResponse> getSchoolList() {
        if (response == null) {
            response = new MutableLiveData<>();
        }

        getSchoolListUseCase.getNYCSchools().subscribe(new DisposableObserver<List<NYCSchool>>() {
            @Override
            public void onNext(List<NYCSchool> nycSchools) {
                Log.d(getClass().getSimpleName(), "nycSchool List : " + nycSchools.size());
                if (!nycSchools.isEmpty()) {
                    response.setValue(new NYCSchoolListResponse(nycSchools, null));
                } else {
                    response.setValue(new NYCSchoolListResponse(null, new RuntimeException()));
                }
            }

            @Override
            public void onError(Throwable e) {
                response.setValue(new NYCSchoolListResponse(null, new RuntimeException()));

            }

            @Override
            public void onComplete() {

            }
        });


        return response;
    }


}
