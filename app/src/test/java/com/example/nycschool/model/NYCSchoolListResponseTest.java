package com.example.nycschool.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class NYCSchoolListResponseTest {

    @Mock
    private List<NYCSchool> mockNycSchoolList;

    @Mock
    private Throwable mockError;

    private NYCSchoolListResponse nycSchoolListResponse;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        nycSchoolListResponse = new NYCSchoolListResponse(mockNycSchoolList, mockError);


    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getNycSchoolList() {
        assertEquals(mockNycSchoolList, nycSchoolListResponse.getNycSchoolList());
    }

    @Test
    public void getError() {
        assertEquals(mockError, nycSchoolListResponse.getError());
    }
}