package com.example.nycschool.usecase;

import com.example.nycschool.model.SchoolData;
import com.example.nycschool.model.SchoolSatData;
import com.example.nycschool.repository.SchoolListRepository;
import com.example.nycschool.repository.SchoolSatDataRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Observable;

import static org.junit.Assert.assertNotNull;

public class GetSchoolListUseCaseTest {

    @Mock
    SchoolListRepository mockSchoolListRespository;

    @Mock
    SchoolSatDataRepository mockSchoolSatDataRepository;

    GetSchoolListUseCase getSchoolListUseCase;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        getSchoolListUseCase = new GetSchoolListUseCase(mockSchoolListRespository, mockSchoolSatDataRepository);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getSchoolSatDataList() {
        getSchoolListUseCase.getSchoolSatDataList();
        assertNotNull(mockSchoolSatDataRepository);
        Mockito.verify(mockSchoolSatDataRepository).getNYCSatDataList();

    }

    @Test
    public void getNYCSchoolList() {
        getSchoolListUseCase.getNYCSchoolList();
        assertNotNull(mockSchoolListRespository);
        Mockito.verify(mockSchoolListRespository).getNYCSchoolList();

    }

    @Test
    public void getNYCSchools() {
        Observable<List<SchoolData>> mockSchoolDataList = Mockito.mock(Observable.class);
        Observable<List<SchoolSatData>> mockSchoolSatDataList = Mockito.mock(Observable.class);
        Mockito.when(mockSchoolListRespository.getNYCSchoolList()).thenReturn(mockSchoolDataList);
        Mockito.when(mockSchoolSatDataRepository.getNYCSatDataList()).thenReturn(mockSchoolSatDataList);
        getSchoolListUseCase.getNYCSchools();
        assertNotNull(mockSchoolSatDataRepository);
        assertNotNull(mockSchoolListRespository);
        Mockito.verify(mockSchoolSatDataRepository).getNYCSatDataList();
        Mockito.verify(mockSchoolListRespository).getNYCSchoolList();

    }
}